// C BASIC - IUP 3D OpenGL
// gcc cbiupgl.c -I/usr/include/iup -liup -liupgl -lGL -lGLU  -o cbiupgl

#include <stdio.h>
#include <stdlib.h>
#include <iup.h>
#include <iupgl.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "cbasic.h"

DIM AS Ihandle PTR canvas;
DIM AS int width = 640, height = 480;
DIM AS float t = 0;

CB_SUB polygon(int a, int b, int c, int d)
BEGIN_SUB
  DIM AS double vertices[][3] = {
    {-1,-1, 1},
    {-1, 1, 1},
    { 1, 1, 1},
    { 1,-1, 1},
    {-1,-1,-1},
    {-1, 1,-1},
    { 1, 1,-1},
    { 1,-1,-1}
    };
  glBegin(GL_POLYGON);
    glVertex3dv(vertices[a]);
    glVertex3dv(vertices[b]);
    glVertex3dv(vertices[c]);
    glVertex3dv(vertices[d]);
  glEnd();
END_SUB

CB_SUB colorCube()
BEGIN_SUB
  glColor3f(1, 0, 0);
  glNormal3f(1, 0, 0);
  polygon(2, 3, 7, 6);

  glColor3f(0, 1, 0);
  glNormal3f(0, 1, 0);
  polygon(1, 2, 6, 5);

  glColor3f(0, 0, 1);
  glNormal3f(0, 0, 1);
  polygon(0, 3, 2, 1);

  glColor3f(1, 0, 1);
  glNormal3f(0, -1, 0);
  polygon(3, 0, 4, 7);

  glColor3f(1, 1, 0);
  glNormal3f(0, 0, -1);
  polygon(4, 5, 6, 7);

  glColor3f(0, 1, 1);
  glNormal3f(-1, 0, 0);
  polygon(5, 4, 0, 1);
END_SUB

CB_FUNCTION repaint_cb(Ihandle PTR self)
BEGIN_FUNCTION
  IupGLMakeCurrent(self);
  glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glTranslatef(0.0f, 0.0f , 0.0f);
  glScalef(1.0f, 1.0f, 1.0f);
  glRotatef(t, 0, 0, 1);
  colorCube();
  glPopMatrix();
  IupGLSwapBuffers(self);
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION resize_cb(Ihandle PTR self, int new_width, int new_height)
BEGIN_FUNCTION
  IupGLMakeCurrent(self);
  glViewport(0, 0, new_width, new_height);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(60, 4./3., 1., 15);
  gluLookAt(3, 3, 3, 0, 0, 0, 0, 0, 1);
  width = new_width;
  height = new_height;
  repaint_cb(canvas);
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION idle_cb()
BEGIN_FUNCTION
  INCR t;
  repaint_cb(canvas);
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION exit_cb()
BEGIN_FUNCTION
  RETURN_FUNCTION(IUP_CLOSE);
END_FUNCTION

FUNCTION Ihandle PTR initDialog()
BEGIN_FUNCTION
  DIM AS Ihandle PTR dialog;
  canvas = IupGLCanvas("repaint_cb");
  IupSetFunction("repaint_cb", (Icallback) repaint_cb);
  IupSetAttribute(canvas,IUP_RASTERSIZE,"640x480");
  IupSetAttribute(canvas,IUP_BUFFER,IUP_DOUBLE);
  IupSetCallback(canvas, "RESIZE_CB",(Icallback) resize_cb);
  dialog = IupDialog(canvas);
  IupSetAttribute(dialog, "TITLE", "C BASIC - IUP 3D OpenGL");
  IupSetCallback(dialog, "CLOSE_CB", (Icallback) exit_cb);
  IupSetFunction (IUP_IDLE_ACTION, (Icallback) idle_cb);
  RETURN_FUNCTION(dialog);
END_FUNCTION

MAIN
BEGIN_FUNCTION
  DIM AS Ihandle PTR dialog;
  IupOpen(AT argc, AT argv);
  IupGLCanvasOpen();
  dialog = initDialog();
  IupShowXY(dialog, IUP_CENTER, IUP_CENTER);
  IupMainLoop();
  IupClose();
  RETURN_FUNCTION(0);
END_FUNCTION

