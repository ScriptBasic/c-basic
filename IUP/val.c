// C BASIC - IUP Val / Mouse Example

#include <stdlib.h>
#include <stdio.h>
#include "cbasic.h"
#include "iup.h"

DIM AS Ihandle PTR lbl_h = NULL, PTR lbl_v = NULL;

CB_FUNCTION mousemove(Ihandle PTR c, double a)
BEGIN_FUNCTION
  DIM AS char buffer[40];
  DIM AS char PTR type = NULL;
  FORMAT(buffer, "VALUE=%.2g", a);
  type = IupGetAttribute(c, "ORIENTATION");
  SELECT_CASE(type[0])
  BEGIN_SELECT
    CASE 'V':
      IupStoreAttribute(lbl_v, "TITLE", buffer);
      END_CASE
    CASE 'H':
      IupStoreAttribute(lbl_h, "TITLE", buffer);
      END_CASE
  END_SELECT
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION button_press(Ihandle PTR c, double a)
BEGIN_FUNCTION
  DIM AS char PTR type = IupGetAttribute(c, "ORIENTATION");
  SELECT_CASE(type[0])
  BEGIN_SELECT
    CASE 'V':
      IupSetAttribute(lbl_v, "FGCOLOR", "255 0 0");
      END_CASE
    CASE 'H':
      IupSetAttribute(lbl_h, "FGCOLOR", "255 0 0");
      END_CASE
  END_SELECT
  mousemove(c, a);
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION button_release(Ihandle PTR c, double a)
BEGIN_FUNCTION
  DIM AS char PTR type = IupGetAttribute(c, "ORIENTATION");
  SELECT_CASE(type[0])
  BEGIN_SELECT
    CASE 'V':
      IupSetAttribute(lbl_v, "FGCOLOR", "0 0 0");
      END_CASE
    CASE 'H':
      IupSetAttribute(lbl_h, "FGCOLOR", "0 0 0");
      END_CASE
  END_SELECT
  mousemove(c, a);
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

MAIN
BEGIN_FUNCTION
  DIM AS Ihandle PTR dlg_val, PTR val_h, PTR val_v;
  IupOpen(AT argc, AT argv);
  val_v = IupVal("VERTICAL");
  val_h = IupVal("HORIZONTAL");
  lbl_v = IupLabel("VALUE=");
  lbl_h = IupLabel("VALUE=");
  IupSetAttribute(lbl_v, "SIZE", "50x");
  IupSetAttribute(lbl_h, "SIZE", "50x");
  IupSetAttribute(val_v, "SHOWTICKS", "5");
  dlg_val = IupDialog(IupHbox(IupSetAttributes(IupHbox(val_v, lbl_v, NULL), "ALIGNMENT=ACENTER"),
    IupSetAttributes(IupVbox(val_h, lbl_h, NULL), "ALIGNMENT=ACENTER"), NULL));
  IupSetCallback(val_v, "BUTTON_PRESS_CB",  (Icallback)button_press);
  IupSetCallback(val_v, "BUTTON_RELEASE_CB",  (Icallback)button_release);
  IupSetCallback(val_v, "MOUSEMOVE_CB", (Icallback)mousemove); 
  IupSetCallback(val_h, "BUTTON_PRESS_CB",  (Icallback)button_press);
  IupSetCallback(val_h, "BUTTON_RELEASE_CB",  (Icallback)button_release);
  IupSetCallback(val_h, "MOUSEMOVE_CB", (Icallback)mousemove); 
  IupSetAttribute(dlg_val, "TITLE", "IupVal");
  IupSetAttribute(dlg_val, "MARGIN", "10x10");
  IupShowXY(dlg_val,IUP_CENTER,IUP_CENTER);
  IupMainLoop();
  IupClose();
  RETURN_FUNCTION(EXIT_SUCCESS);
END_FUNCTION

