// C BASIC - IUP Web Browser Control
// gcc webrowser.c -I/usr/include/iup -liup -liupweb -o cbwebctrl

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "iup.h"
#include "iupweb.h"
#include "cbasic.h"

#ifndef WIN32
CB_FUNCTION history_cb(Ihandle PTR ih)
BEGIN_FUNCTION
  DIM AS int i;
  DIM AS char str[50];
  DIM AS int back = IupGetInt(ih, "BACKCOUNT");
  DIM AS int fwrd = IupGetInt(ih, "FORWARDCOUNT");
  PRINT ("HISTORY ITEMS\n");
  FOR (i = -(back) TO i < 0 STEP INCR i)
  BEGIN_FOR
    FORMAT(str, "ITEMHISTORY%d", i);
    PRINT ("Backward %02d: %s\n", i, IupGetAttribute(ih, str));
  NEXT
  FORMAT(str, "ITEMHISTORY%d", 0);
  PRINT ("Current  %02d: %s\n", 0, IupGetAttribute(ih, str));
  FOR (i = 1 TO i <= fwrd STEP INCR i)
  BEGIN_FOR
    FORMAT(str, "ITEMHISTORY%d", i);
    PRINT ("Forward  %02d: %s\n", i, IupGetAttribute(ih, str));
  NEXT
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION
#endif

CB_FUNCTION navigate_cb(Ihandle PTR self, char PTR url)
BEGIN_FUNCTION
  PRINT ("NAVIGATE_CB: %s\n", url);
  (void)self;
  IF (strstr(url, "download") != NULL) THEN_DO RETURN_FUNCTION(IUP_IGNORE);
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION error_cb(Ihandle PTR self, char PTR url)
BEGIN_FUNCTION
  PRINT ("ERROR_CB: %s\n", url);
  (void)self;
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION completed_cb(Ihandle PTR self, char PTR url)
BEGIN_FUNCTION
  PRINT ("COMPLETED_CB: %s\n", url);
  (void)self;
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION newwindow_cb(Ihandle PTR self, char PTR url)
BEGIN_FUNCTION
  PRINT ("NEWWINDOW_CB: %s\n", url);
  IupSetAttribute(self, "VALUE", url);
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION back_cb(Ihandle PTR self)
BEGIN_FUNCTION
  Ihandle PTR web  = (Ihandle PTR)IupGetAttribute(self, "MY_WEB");
  IupSetAttribute(web, "BACKFORWARD", "-1");
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION forward_cb(Ihandle PTR self)
BEGIN_FUNCTION
  Ihandle PTR web  = (Ihandle PTR)IupGetAttribute(self, "MY_WEB");
  IupSetAttribute(web, "BACKFORWARD", "1");
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION stop_cb(Ihandle PTR self)
BEGIN_FUNCTION
  Ihandle PTR web  = (Ihandle PTR)IupGetAttribute(self, "MY_WEB");
  IupSetAttribute(web, "STOP", NULL);
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION reload_cb(Ihandle PTR self)
BEGIN_FUNCTION
  Ihandle PTR web  = (Ihandle PTR)IupGetAttribute(self, "MY_WEB");
  IupSetAttribute(web, "RELOAD", NULL);
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

CB_FUNCTION load_cb(Ihandle PTR self)
BEGIN_FUNCTION
  Ihandle PTR txt  = (Ihandle PTR)IupGetAttribute(self, "MY_TEXT");
  Ihandle PTR web  = (Ihandle PTR)IupGetAttribute(self, "MY_WEB");
  IupSetAttribute(web, "VALUE", IupGetAttribute(txt, "VALUE"));
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

SUB WebBrowserTest()
BEGIN_SUB
  DIM AS Ihandle PTR txt,
                 PTR dlg,
                 PTR web,
                 PTR btLoad,
                 PTR btReload,
                 PTR btBack,
                 PTR btForward,
                 PTR btStop;
#ifndef WIN32
  DIM AS Ihandle PTR history;
#endif
  IupWebBrowserOpen();
  web = IupWebBrowser();
  dlg = IupDialog(IupVbox(IupHbox(btBack = IupButton("Back", NULL),
                                  btForward = IupButton("Forward", NULL),
                                  txt = IupText(""),
                                  btLoad = IupButton("Load", NULL),
                                  btReload = IupButton("Reload", NULL),
                                  btStop = IupButton("Stop", NULL),
#ifndef WIN32
                                  history = IupButton("History", NULL),
#endif
                                  NULL),
                                  web, NULL));
  IupSetAttribute(dlg, "TITLE", "C BASIC - IUP Web Browser Control");
  IupSetAttribute(dlg, "MY_TEXT", (char PTR)txt);
  IupSetAttribute(dlg, "MY_WEB", (char PTR)web);
  IupSetAttribute(dlg, "RASTERSIZE", "800x600");
  IupSetAttribute(dlg, "MARGIN", "10x10");
  IupSetAttribute(dlg, "GAP", "10");
  IupSetAttribute(txt, "VALUE", "https://bitbucket.org/ScriptBasic/c-basic");
  IupSetAttribute(web, "VALUE", IupGetAttribute(txt, "VALUE"));
  IupSetAttributeHandle(dlg, "DEFAULTENTER", btLoad);
  IupSetAttribute(txt, "EXPAND", "HORIZONTAL");
  IupSetCallback(btLoad, "ACTION", (Icallback)load_cb);
  IupSetCallback(btReload, "ACTION", (Icallback)reload_cb);
  IupSetCallback(btBack, "ACTION", (Icallback)back_cb);
  IupSetCallback(btForward, "ACTION", (Icallback)forward_cb);
  IupSetCallback(btStop, "ACTION", (Icallback)stop_cb);
#ifndef WIN32
  IupSetCallback(history, "ACTION", (Icallback)history_cb);
#endif
  IupSetCallback(web, "NEWWINDOW_CB", (Icallback)newwindow_cb);
  IupSetCallback(web, "NAVIGATE_CB", (Icallback)navigate_cb);
  IupSetCallback(web, "ERROR_CB", (Icallback)error_cb);
  IupSetCallback(web, "COMPLETED_CB", (Icallback)completed_cb);
  IupShow(dlg);
END_SUB

MAIN
BEGIN_FUNCTION
  IupOpen(AT argc, AT argv);
  WebBrowserTest();
  IupMainLoop();
  IupClose();
  RETURN_FUNCTION(EXIT_SUCCESS);
END_FUNCTION

