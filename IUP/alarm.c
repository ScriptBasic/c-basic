// C BASIC - Iup Alarm
// gcc alarm.c -I/usr/include/iup -liup -o alarm

#include <stdlib.h>
#include <stdio.h>
#include "iup.h"
#include "cbasic.h"

MAIN
BEGIN_FUNCTION
  IupOpen(AT argc, AT argv);
  SELECT_CASE (IupAlarm ("C BASIC - IUP Alarm", "Save file before exit?", "Yes", "No", "Cancel"))
  BEGIN_SELECT
    CASE 1:
      IupMessage ("Save file", "File saved successfully - leaving program");
      END_CASE
    CASE 2:
      IupMessage ("Save file", "File not saved - leaving program anyway");
      END_CASE
    CASE 3:
      IupMessage ("Save file", "Operation canceled");
      END_CASE
  END_SELECT
  IupClose();
  RETURN(EXIT_SUCCESS);
END_FUNCTION
