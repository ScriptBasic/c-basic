// C BASIC IUP Matrix List
// gcc cbmatrixlist.c -I/usr/include/iup -I/usr/include/cd -liup -liupcd -liupcontrols -o cbmatrixlist

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "iup.h"
#include "iupcontrols.h"
#include "cd.h"
#include "cbasic.h"


CB_FUNCTION listclick_cb(Ihandle PTR self, int lin, int col, char PTR status)
BEGIN_FUNCTION
  DIM AS char PTR value = IupGetAttributeId(self, "", lin);
  IF (NOT value) THEN_DO value = "NULL";
  PRINT("click_cb(%d, %d)\n", lin, col);
  PRINT("  VALUE%d:%d = %s\n", lin, col, value);
  RETURN_FUNCTION(IUP_DEFAULT);
END_FUNCTION

MAIN
BEGIN_FUNCTION
  DIM AS Ihandle PTR dlg, PTR mlist;
  IupOpen(AT argc, AT argv);       
  IupControlsOpen();
  mlist = IupMatrixList();
  IupSetInt(mlist, "COUNT", 10);
  IupSetInt(mlist, "VISIBLELINES", 5);
  IupSetAttribute(mlist, "COLUMNORDER", "LABEL:COLOR:IMAGE");
  IupSetAttribute(mlist, "EDITABLE", "Yes");
  IupSetAttribute(mlist, "TITLE", "Test");
  IupSetAttribute(mlist, "BGCOLOR", "220 230 240");
  IupSetAttribute(mlist, "FRAMECOLOR", "120 140 160");
  IupSetAttribute(mlist, "ITEMBGCOLOR0", "120 140 160");
  IupSetAttribute(mlist, "ITEMFGCOLOR0", "255 255 255");
  IupSetAttribute(mlist, "1", "AAA");
  IupSetAttribute(mlist, "2", "BBB");
  IupSetAttribute(mlist, "3", "CCC");
  IupSetAttribute(mlist, "4", "DDD");
  IupSetAttribute(mlist, "5", "EEE");
  IupSetAttribute(mlist, "6", "FFF");
  IupSetAttribute(mlist, "7", "GGG");
  IupSetAttribute(mlist, "8", "HHH");
  IupSetAttribute(mlist, "9", "III");
  IupSetAttribute(mlist, "10","JJJ");
  IupSetAttribute(mlist, "COLOR1", "255 0 0");
  IupSetAttribute(mlist, "COLOR2", "255 255 0");
  IupSetAttribute(mlist, "COLOR4", "0 255 255");
  IupSetAttribute(mlist, "COLOR5", "0 0 255");
  IupSetAttribute(mlist, "COLOR6", "255 0 255");
  IupSetAttribute(mlist, "COLOR7", "255 128 0");
  IupSetAttribute(mlist, "COLOR8", "255 128 128");
  IupSetAttribute(mlist, "COLOR9", "0 255 128");
  IupSetAttribute(mlist, "COLOR10", "128 255 128");
  IupSetAttribute(mlist, "ITEMACTIVE3", "NO");
  IupSetAttribute(mlist, "ITEMACTIVE7", "NO");
  IupSetAttribute(mlist, "ITEMACTIVE8", "NO");
  IupSetAttribute(mlist, "IMAGEACTIVE9", "No");
  IupSetAttribute(mlist, "IMAGEVALUE1", "ON");
  IupSetAttribute(mlist, "IMAGEVALUE2", "ON");
  IupSetAttribute(mlist, "IMAGEVALUE3", "ON");
  dlg = IupDialog(IupVbox(mlist, NULL));
  IupSetAttribute(dlg, "TITLE", "IupMatrixList");
  IupSetAttribute(dlg, "MARGIN", "10x10");
  IupSetCallback(mlist,"LISTCLICK_CB",(Icallback)listclick_cb);
  IupShowXY(dlg, IUP_CENTER, IUP_CENTER);
  IupMainLoop();
  IupClose();  
  RETURN_FUNCTION(EXIT_SUCCESS);
END_FUNCTION

