// C BASIC - cURL (wget example)
// gcc cbcurl.c -lcurl -o cbcurl

#include <stdio.h>
#include <curl/curl.h>
#include "cbasic.h"
 
MAIN
BEGIN_FUNCTION
  DIM AS CURL *curl;
  DIM AS CURLcode res;
  curl = curl_easy_init();
  IF (curl) THEN
    curl_easy_setopt(curl, CURLOPT_URL, "localhost/index.html");
    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  END_IF
  RETURN_FUNCTION(0);
END_FUNCTION
