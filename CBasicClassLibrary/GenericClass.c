/*

  Charles E V Pegge  
  12:18 08/11/2013

  PROPERTIES:
  ===========

  * Standard header base used for all classes of object

  * Expandable Array of Objects as single entity

  * Array buffer can be lerger than contents, to minimise memory reallocations

  * Array buffer is terminated by 2 extra null bytes (supporting wide character arrays)


  STRUCTURE:
  ==========

  BASE-HEADER
  ADDITIONAL MEMBERS
  OBJECT ARRAY BUFFER

*/


  #define FreeObject  free
  #define ObjectOffset(A) A + A->offset
  #define ObjectHeaderBytes(A) A->offset
  #define ObjectAllocBytes(A) A->offset + A->nbytes + 2


  typedef struct ClassTableStruct ClassTable,*Methods;

  types ClassStruct
  begin
  ref f;      // pointer to methods/function table
  int type;   // dynamic type code
  int offset; // size of this header (jump to data)
  int nbytes; // buffer size for body of data
  int count;  // number of elements
  int size;   // size of each element
  end
  Class,*Object;



  //GENERIC OBJECTS
  
  function ref   Transfer        (ref*r, ref s);
  function ref   Stretch         (ref*pt,int n);
  function ref   Shrink          (ref*pt, int n);
  function ref   Truncate        (ref*pt, int n);
  sub            SetObjectHeader (Object this,ref f,int le, int wi,int so);
  function ref   NewObject       (ref f, int nc, int wi, int so);


  dim Methods pMethods=0;


  function ref Transfer(ref*r, ref s)
  begin
    if (*r==s) return s;   // no transfer
    if (*r) then free(*r); // free old
    *r=s;                  // ref new
    return s;              // return new
  end


  function ref Stretch(ref*pt,int n)
  begin
    Object t=*pt;
    int mm,nn;
    if (t==0) then return 0;
    mm=t->count * t->size + t->offset;
    nn=n * t->size + t->offset;
    if (nn > t->nbytes) then AdjustBuffer(t, mm, nn+2);
    return Transfer(pt,t);
  end


  function ref Shrink(ref*pt, int n)
  begin
    Object t=*pt;
    int mm,nn;
    if (t==0) then return 0;
    mm=t->count * t->size + t->offset;
    nn=n * t->size + t->offset;
    if (nn<mm) then nn=mm; //clamp minimum nn
    if (nn < t->nbytes) then AdjustBuffer(t, mm, nn+2);
    return Transfer(pt, t);
  end


  function ref Truncate(ref*pt, int n)
  begin
    Object t=*pt;
    int mm,nn;
    if (t==0) then return 0;
    mm=t->offset;
    nn=n * t->size + t->offset;
    if (nn<mm) then nn=mm; //clamp minimum nn
    if (nn < t->nbytes) then AdjustBuffer(t, mm, nn+2);
    return Transfer(pt, t);
  end


  sub SetObjectHeader(Object this,ref f,int le, int wi,int so)
  begin
    this->f      = f;
    this->offset = so;
    this->nbytes = le*wi;
    this->count  = le;
    this->size  = wi;
  end


  function ref NewObject(ref f, int nc, int wi, int so)
  begin
    Object this = NewSpace(nc*wi+so+2);
    SetObjectHeader(this, f, nc, wi,so);
    return this;
  end




