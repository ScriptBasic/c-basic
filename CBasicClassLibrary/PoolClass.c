
/*

  POOL CLASS 
  =========================

  for one-time storage of strings
  and other constructs within a single
  pool space.

*/

//types PoolMethodsStruct PoolMethodsTable,*PoolMethods;

  types PoolClassStruct
  begin
//BASE FIELDS
  ref f;              // pointer to methods table
  int type;           // dynamic type code
  int offset;         // size of this header
  int nbytes;         // buffer size 
  int count;          // number of bytes used
  int size ;          // size (1) measuring in bytes
  end
  PoolClass, *PoolObject;



  function PoolObject NewPool(int n)
  begin
    PoolObject e;
    n+=sizeof(PoolClass);
    e = NewSpace(n);
    e->nbytes=n;
    e->offset=sizeof(PoolClass);
    e->count=0;
    e->size=1;
    return e;
  end


  function int PoolAlloc(PoolObject *pt, int n)
  begin
    int q,b;
    PoolObject p=*pt;
    if (n + p->count > p->nbytes)
    begin
      q = p->offset + p->count;
      AdjustBuffer(p, q, q + n + 16000);
      p->nbytes+= p->count + n + 16000;
    end
    b = p->offset + p->count;
    p->count += n;
    FillBytes((char*)p + b,  n, 0);
    Transfer((ref*)pt, (ref)p);
    return b;
  end



  function int PoolGetBoundary(PoolObject *pt)
  begin
    PoolObject p=*pt;
    return p->offset + p->count;
  end


  function int PoolSetBoundary(PoolObject *pt, int i)
  begin
    PoolObject p=*pt;
    p->count=i - p->offset;
    return i;
  end

