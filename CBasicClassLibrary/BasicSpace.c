
  #define FreeSpace   free

  function int   ByteLen(char*s);
  function int   WordLen(char*s);
  function ref   NewSpace        (int count);
  sub            CopyBytes       (ref tv,ref sv,int count);
  sub            NullTerminate   (char*s);
  function int   FillBytes       (char*s, int le, char c);
  function ref   AdjustBuffer    (ref s,int m, int n);


  function int ByteLen(char*s)
  begin
    int i=0;
    while (s[i] !=0 ) i++;
    return i;
  end


  function int WordLen(char*s)
  begin
    short*w=(short*) s;
    int i=0;
    while (w[i] !=0 ) i++;
    return i;
  end


  function ref NewSpace(int count)
  begin
    char *v = (char*) malloc(count);
    int i;
    for (i=0; to i<count; step incr i) v[i]=0; // set chars to null
    return v;
  end


  sub CopyBytes(ref tv,ref sv,int count)
  begin
    char*s=sv;
    char*t=tv;
    int i;
    for (i=0; to i<count; step incr i)
    begin
      *t=*s;
   incr t;
      incr s;
    end
  end


  sub NullTerminate(char*s)
  begin
    s[0]=0;
    s[1]=0;
  end


  function int FillBytes(char*s, int le, char c)
  begin
    int i;
    for (i=0; to i<le; incr i)
    begin
      *s=c;
      incr s;
    end
    return 1;
  end


  function ref AdjustBuffer(ref s,int m, int n)
  begin
    char*t = (char*) malloc(n);
    if (m>n)  then m=n; //clamp m
    if (s==0) then m=0;
    if (m)    then CopyBytes(t,s,m);
    FillBytes(t+m,n-m,0);
    return t;
  end


