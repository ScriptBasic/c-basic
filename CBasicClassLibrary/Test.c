
  #include "BasicClassLibrary.h"


  function TestStrings()
  begin

    dim as double v;
    dim as StringObject  uo,vo,wo;    
    dim as WordObject ww;
    dim as StringMethods f;

    CharWidth=1;

    uo=NewString(0);
    vo=NewString(0);
    wo=NewString(100);

    //((StringMethods)vo->f)->Show(&vo); //ugh!

    f=(StringMethods)(vo->f);

    f->SetChars(&uo,"LO");
    f->SetChars(&vo," HellO ");
    f->Ltrim(&vo,vo);
    f->Rtrim(&vo,vo);
    f->Ucase(&vo,vo);
    f->Join(&wo,3,vo,vo,vo);
    f->Show(&uo);
    f->Show(&vo);
    f->Show(&wo);
    printf("%i\n",f->Instr(&wo,6,uo));
    f->Lcase(&wo,wo);
    f->Insert(&wo,uo,6);
    f->Show(&wo);
    f->Delete(&wo,6,2);
    f->Show(&wo);
    f->Str(&vo,42.0);
    f->Show(&vo);
    v=f->Val(&vo);
    printf("%f\n",v);
    f->SetChars(&vo,"Abc");
    f->Repeat(&wo,vo,7);
    f->Show(&wo);
    f->Replace(&wo,vo,uo);
    f->Show(&wo);
    f->Replace(&wo,uo,vo);
    f->Show(&wo);
    f->SetChars(&uo,"<");
    f->SetChars(&vo,">");
    f->SetChars(&wo,"  Hello World! ");
    f->Show(&uo);
    f->Show(&vo);
    f->Show(&wo);
    f->Join(&wo,3,uo,f->Ltrim(&wo,f->Rtrim(&wo,f->Ucase(&wo,wo))),vo );
    f->Show(&wo);
    f->Join(&wo,3,uo,wo,vo);
    f->Copy(&vo,wo);
    f->Show(&vo);
    f->GetMid(&vo,wo,3,6);
    f->Show(&vo);
    f->SetMid(&vo,6,vo);
    f->Show(&vo);
    f->SetChars(&vo,"abc\1\2\3\4\14\\abc");
    f->Show(&vo);

//  itr string array storage transfers

    f->SetChars(&vo,"  aa=b+c");
    ww=NewWord(&vo,256);
    NextBasicWord(&ww);
    WordInfo(&ww);
    NextBasicWord(&ww);
    WordShow(&ww);

    StringFree(&uo);
    StringFree(&vo);
    StringFree(&wo);
    WordFree(&ww);
  end
    

  function int main()
  begin
    TestStrings();
  end
