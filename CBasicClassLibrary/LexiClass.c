
/*

  LEXING
  ======

*/

  /* Word -> x flags */  

  #define IsNull    1
  #define LineBreak 2
  #define IsWord    4
  #define IsNumber  8
  #define IsAlpha   16
  #define IsQuote   32
  #define IsSymbol  64
  #define Comment  128


  types WordClassStruct
  begin
//BASE MEMBERS
  ref f;              // pointer to methods table
  int type;           // dynamic type code
  int offset;         // size of this header
  int nbytes;         // buffer size for body of data
  int count;          // number of elements
  int size ;          // size of each element
//ADDITIONAL MEMBERS
  StringObject text;  // string buffer
  char* bp;           // start of word
  char* ep;           // end boundary of word
  char* et;           // end boundary of text
  char* next;         // next word
  int   xx;           // status bits
  end
  WordClass, *WordObject;


  function char* WordInfo(WordObject*pw)
  begin
    WordObject w=*pw;
    char*s=(char*) w + w->offset;
  //printf("TEXT:       %s\n",w->bp);
    printf("WORD:       %s\n",s);
    printf("Class Size: %i\n",w->offset);
    printf("Char Width: %i\n",w->size);
    printf("Word Len:   %i\n",w->count);
    printf("Word Start: %i\n",w->bp);
    printf("Word End:   %i\n",w->ep);
    printf("Text End:   %i\n",w->et);
    return 0; //itr
  end

  function char* WordShow(WordObject*pw)
  begin
    WordObject w=*pw;
    char*s=(char*) w + w->offset;
    printf("%s\n",s);
    return 0; //itr
  end

  function WordObject NewWord(StringObject*pText, int n)
  begin
    StringObject text=*pText;
    WordObject w;
    if (n<=0) n=64;
    w = (WordObject) NewSpace(sizeof(WordClass)+n*text->size);
    w->nbytes=n * text->size;
    w->offset=sizeof(WordClass);
    w->count=n;
    w->size=text->size;
    w->text=text;
    w->bp  =(char*) text+text->offset;
    w->next=(char*) text+text->offset;
    w->et  =(char*) text + text->offset + (text->count * text->size);
    return w;
  end


  method WordObject WordFree(WordObject*pthis)
  begin
    WordObject this=*pthis;
    if (this==0) FreeSpace((ref)this);
    *pthis=0;
    return 0;
  end


  #define IfRange(A,B,C) if ((A<=B)&&(B<=C))

  function WordObject SkipSpace(WordObject w)
  begin
    char a;
    char* n = w->et;
    while (w->next < n)
    begin
      a = *w->next;
      IfRange(33,a,255) {w->xx |= IsWord; break;}
      ++w->next;
    end
    return w;
  end


  function WordObject SkipSpaceL(WordObject w)
  begin
    char a;
    char* n = w->et;
    while (w->next < n)
    begin
      a = *w->next;
      if((a==10)or(a==13)) {w->xx |= LineBreak; break;}
      IfRange(33,a,255)    {w->xx |= IsWord;    break;}
      ++w->next;
    end
    return w;
  end


  function WordObject NextSpace(WordObject w)
  begin
    char a;
    char* n = w->et;
    while (w->next < n)
    begin
      a = *w->next;
      if((a==10)or(a==13)) {w->xx |= LineBreak; break;}
      IfRange(0,a,32)      {break;}
      ++w->next;
    end
    return w;
  end


  function WordObject EndLine(WordObject w)
  begin
    char a;
    char* n = w->et;
    while (w->next < n)
    begin
      a = *w->next;
      if((a==10)or(a==13)) {w->xx |= LineBreak; break;}
      ++w->next;
    end
    return w;
  end


  function char* SkipQuoteMarks(WordObject w)
  begin
    char *n;
    char c=*w->next;
    w->next++;
    n = w->et -1;
    while ((w->next < n)and(c != *w->next))
    begin
      ++w->next;
    end
    return w->next+1;
  end


  function WordObject NextBasicWord(WordObject*pw)
  begin
    char a;
    char *n,*k;
    int le;
    WordObject w=*pw;
    if (w==0) then return 0;
    SkipSpace(w);
    w->xx = 0;
    w->bp=w->next;
    while(1)
    begin
      a=*w->bp;
      IfRange(0x30,a,0x39)  {w->xx |= IsNumber; break;}
      IfRange (0x40,a,0x5A) {w->xx |= IsAlpha;  break;}
      IfRange (0x60,a,0x7A) {w->xx |= IsAlpha;  break;}
      if(a==95)             {w->xx |= IsAlpha;  break;}
      if(a==0)              {                  break;}
      if(a==34)             {w->xx |=IsQuote;  break;}
      if(a==96)             {w->xx |=IsQuote;  break;}
      w->xx |=IsSymbol; 
      break;
    end
    n = w->et;
    while (w->next < n)
    begin
      a=*w->next;
      ++w->next;
      IfRange (0x30,a,0x39) {continue;}
      IfRange (0x40,a,0x5A) {continue;}
      IfRange (0x60,a,0x7A) {continue;}
      if(a==95)             {continue;}
      IfRange(0x00,a,0x20)  {--w->next; break;}
      if(a==34)             {--w->next; SkipQuoteMarks(w); break;}
      if(a==96)             {--w->next; SkipQuoteMarks(w); break;}
      //default //symbols
      if(w->bp+1 != w->next) --w->next;
      break;
    end;
    w->ep = w->next;
    w->count = w->ep - w->bp;
    le=w->count * w->size;
    if (le > w->nbytes) //stretch word buffer
    begin
      char*t = malloc(w->offset + w->nbytes + 34);
      CopyBytes(t, w, w->offset);
      free(w);
      w = (ref) t;
    end
    k = (char*) w + w->offset;
    CopyBytes(k, w->bp, le);
    NullTerminate(k+le);
    SkipSpaceL(w);
    return Transfer((ref*)pw, (ref)w);
  end

//itr case conversion
