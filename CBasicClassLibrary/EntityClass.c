
/*

  ENTITY CLASS FOR COMPILER
  =========================

  Entities may have members
  Members may have members

*/

  /* Metatypes */  

  #define IsDefine     0x1000
  #define IsConstant   0x2000
  #define IsComment    0x3000
  #define IsEnum       0x4000
  #define IsVar        0x5000
  #define IsOperator   0x6000
  #define IsProc       0x7000
  #define IsType       0x8000
  #define IsTypeDef    0x9000
  #define IsClass      0xA000

  #define IsDirect     0x0000
  #define IsIndirect1  0x0100
  #define IsIndirect2  0x0200
  #define IsIndirect3  0x0300
  #define IsIndirect4  0x0400

  #define IsNumber     0x0010
  #define IsString     0x0020
  #define IsPrimitive  0x0030
  #define IsInteger    0x0040
  #define IsFloat      0x0050

  #define IsNull       0x0000
  #define IsByte       0x0001
  #define IsShort      0x0002
  #define IsWord       0x0002
  #define IsLong       0x0004
  #define IsDword      0x0004
  #define IsSingle     0x0004
  #define IsDouble     0x0008
  #define IsExtended   0x000A


  types NameClassStruct
  begin
              //no Methods pointer
  char count; //number of bytes
  end
  NameClass,*NameObject;
//name characters follow immediately  (byte packed)


  types  EntityClassStruct
  begin
//BASE FIELDS
  ref f;              // pointer to methods table
  int type;           // dynamic type code
  int offset;         // size of this header
  int nbytes;         // buffer size for body of data
  int count;          // number of members
  int size ;          // size of each member
//ADDITIONAL FIELDS
  int name;
  int metatype;
  end
  EntityClass, *EntityObject;
//block of member references follow


  types  MemberClassStruct
  begin
//BASE FIELDS
  ref f;              // pointer to methods table
  int type;           // dynamic type code
  int offset;         // size of this header
  int nbytes;         // buffer size for body of data
  int count;          // number of submember records (procedure prototype parameters)
  int size ;          // size of each submember (procedure protype parameters)
//ADDITIONAL FIELDS
  int name;         // name of member
  int metatype;
  end
  MemberClass, *MemberObject;
//block of sub-member references follow


//GLOBALS

  PoolObject grp; // general records pool



  #define RefFromToken(I) (ref) grp + grp->offset + I
  #define TokenFromRef(I) grp->offset + I
  #define AllocToken(I) PoolAlloc(&grp, I)

  typedef int NameToken, EntityToken, MemberToken;


  function int NameToken(char*s)
  begin
    int n=ByteLen(s);
    int i=AllocToken(n+2); // for length & string & null 
    PoolObject p=grp;
    char* t=(ref) p + i;
    t[0]=n;
    CopyBytes(t+1, s ,n+1);
    return i; 
  end


  function int NewEntityToken(int n) // nMembers
  begin
    int q = n * sizeof(int); // tokens
    int i=AllocToken(q + sizeof(EntityClass));
    EntityObject e=(ref)grp+i;
    e->offset=sizeof(EntityClass);
    e->count=n; // members of typedef struct
    e->size=sizeof(int);
    return i;
  end


  function int NewMemberToken(int n) // nMembers
  begin
    int q = n * sizeof(int); // indirect
    int i=AllocToken(q + sizeof(MemberClass));
    MemberObject e=(ref)grp+i;
    e->nbytes=q + Sizeof(MemberClass);
    e->offset=sizeof(MemberClass);
    e->count=n; // parameters of procedures
    e->size=sizeof(int);
    return i;
  end


