
  #define function
  #define method
  #define gosub
  #define dim
  #define as
  #define to
  #define step
  #define then
  #define sub   void
  #define begin {
  #define end   }
  #define and   &&
  #define or    ||
//#define class typedef struct
  #define types typedef struct
  #define member ->
  #define addressof &
  #define incr ++
  #define decr --
  #define byref *
  #define ref   void*
  #define references = &

  #define FUNCTION
  #define BEGIN_FUNCTION {
  #define END_FUNCTION }
  #define SUB void
  #define BEGIN_SUB {	
  #define END_SUB }
  #define RETURN return
  #define CALL
  #define AND &&
  #define OR ||
  #define MOD %
  #define DIM	
  #define AS
  #define LET
  #define INCR ++
  #define DECR --
  #define IF if
  #define BEGIN_IF {
  #define THEN {
  #define THEN_DO
  #define ELSE } else {	
  #define END_IF }
  #define FOR for
  #define TO ;
  #define STEP ;
  #define BEGIN_FOR {
  #define NEXT }
  #define SELECT_CASE switch
  #define BEGIN_SELECT {
  #define CASE case	
  #define _TO_ ...
  #define END_CASE break;
  #define CASE_ELSE default:
  #define END_SELECT }
  #define DO do {
  #define WHILE } while

  #define HiWord(n) ((n>>16)and 0xffff)
  #define LoWord(n) (n and 0xffff)


