// C BASIC - SCEW (Simple C Expat Wrapper)
// gcc scew_write.c -lscew -o scew_write

#include <stdio.h>
#include <scew/scew.h>
#include "cbasic.h"

enum {MAX_OUTPUT_BUFFER_ = 2000};

MAIN
BEGIN_FUNCTION
  DIM AS scew_tree PTR tree = NULL;
  DIM AS scew_element PTR root = NULL;
  DIM AS scew_element PTR element = NULL;
  DIM AS scew_element PTR sub_element = NULL;
  DIM AS scew_element PTR sub_sub_element = NULL;
  DIM AS scew_attribute PTR attribute = NULL;
  DIM AS scew_printer PTR printer = NULL;
  DIM AS scew_writer PTR writer = NULL;
  DIM AS XML_Char PTR buffer = NULL;
  IF (argc < 2) THEN
    scew_printf(_XT("Usage: scew_write new_file.xml\n"));
    RETURN_FUNCTION(EXIT_FAILURE);
  END_IF
  tree = scew_tree_create ();
  root = scew_tree_set_root (tree, _XT("test"));
  element = scew_element_add (root, _XT("element"));
  scew_element_set_contents (element, _XT("element contents"));
  element = scew_element_add (root, _XT("element"));
  scew_element_add_attribute_pair (element, _XT("attribute"), _XT("value"));
  element = scew_element_add (root, _XT("element"));
  scew_element_add_attribute_pair (element, _XT("attribute1"), _XT("value1"));
  attribute = scew_attribute_create (_XT("attribute2"), _XT("value2"));
  scew_element_add_attribute (element, attribute);
  element = scew_element_add (root, _XT("element"));
  sub_element = scew_element_add (element, _XT("subelement"));
  scew_element_add_attribute_pair (sub_element, _XT("attribute"), _XT("value"));
  sub_element = scew_element_add (element, _XT("subelement"));
  scew_element_add_attribute_pair (sub_element, _XT("attribute1"), _XT("value1"));
  scew_element_add_attribute_pair (sub_element, _XT("attribute2"), _XT("value2"));
  sub_sub_element = scew_element_add (sub_element, _XT("subsubelement"));
  scew_element_add_attribute_pair (sub_sub_element, _XT("attribute1"), _XT("value1"));
  scew_element_add_attribute_pair (sub_sub_element, _XT("attribute2"), _XT("value2"));
  scew_element_add_attribute_pair (sub_sub_element, _XT("attribute3"), _XT("value3"));
  scew_element_add_attribute_pair (sub_sub_element, _XT("attribute2"), _XT("new_value2"));
  scew_element_set_contents (sub_sub_element, _XT("With accents: à é è í ó ú"));
  writer = scew_writer_file_create (argv[1]);
  IF (writer EQ NULL) THEN
    PRINT ("Unable to create %s\n", argv[1]);
    RETURN_FUNCTION(EXIT_FAILURE);
  END_IF
  printer = scew_printer_create (writer);
  IF (printer EQ NULL) THEN
    PRINT ("Unable to create printer\n");
    RETURN_FUNCTION(EXIT_FAILURE);
  END_IF
  (void) scew_printer_print_tree (printer, tree);
  scew_writer_free (writer);
  buffer = (XML_Char *) malloc (MAX_OUTPUT_BUFFER_);
  writer = scew_writer_buffer_create (buffer, MAX_OUTPUT_BUFFER_);
  IF (writer EQ NULL) THEN
    PRINT ("Unable to create writer buffer\n");
    RETURN_FUNCTION(EXIT_FAILURE);
  END_IF
  scew_printer_set_writer (printer, writer);
  (void) scew_printer_print_tree (printer, tree);
  scew_printf (_XT("%s"), buffer);
  free (buffer);
  scew_writer_free (writer);
  scew_printer_free (printer);
  scew_tree_free (tree);
  RETURN_FUNCTION(0);
END_FUNCTION

