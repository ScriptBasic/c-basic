#include "cbasic.h"

FUNCTION int main()
BEGIN_FUNCTION
  DIM AS int x;
  FOR (x = 1 TO x < 6 STEP INCR x)
  BEGIN_FOR
    PRINT ("%i\n",x);
  NEXT
  IF (x == 6) THEN_DO PRINT ("x is now 6\n");
  IF (x > 5 AND x < 7) THEN
    PRINT ("My guess is 6\n");
  ELSE
    PRINT ("Wrong!\n"); 
  END_IF
  LET int y = 0;
  DO
    INCR y;
  WHILE (y < x);
  PRINT ("%i\n", y);
  x += y;
  SELECT_CASE (x)
  BEGIN_SELECT
    CASE 6:
      PRINT ("I remember 6\n");
      END_CASE
    CASE 12:
      PRINT ("Double it up\n");
      END_CASE
    CASE_ELSE
      PRINT ("Some other number\n");
  END_SELECT 
  RETURN_FUNCTION(0);
END_FUNCTION

