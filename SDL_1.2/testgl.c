// C BASIC - SDL - OpenGL 
/*  gcc -o testgl testgl.c -g -O2 -I/usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT -DHAVE_OPENGL -L/usr/lib/x86_64-linux-gnu -lSDL -lGL -lm  */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <SDL/SDL.h>

#ifdef __MACOS__
#define HAVE_OPENGL
#endif

#ifdef HAVE_OPENGL

#include <SDL/SDL_opengl.h>

/* Undefine this IF you want a flat cube instead of a rainbow cube */
#define SHADED_CUBE

/* Define this to be the name of the logo image to use with -logo */
#define LOGO_FILE "icon.bmp"

/* The SDL_OPENGLBLIT interface is deprecated.
   The code is still available FOR benchmark purposes though.
*/

#include "cbasic.h"

DIM AS static SDL_bool USE_DEPRECATED_OPENGLBLIT = SDL_FALSE;

DIM AS static SDL_Surface PTR global_image = NULL;
DIM AS static GLuint global_texture = 0;
DIM AS static GLuint cursor_texture = 0;

/**********************************************************************/

SUB HotKey_ToggleFullScreen()
BEGIN_SUB
  DIM AS SDL_Surface PTR screen;
  screen = SDL_GetVideoSurface();
  IF (SDL_WM_ToggleFullScreen(screen)) THEN
    PRINT("Toggled fullscreen mode - now %s\n",(screen->flags & SDL_FULLSCREEN) ? "fullscreen" : "windowed");
  ELSE
    PRINT("Unable to toggle fullscreen mode\n");
  END_IF
END_SUB

SUB HotKey_ToggleGrab()
BEGIN
  DIM AS SDL_GrabMode mode;
  PRINT("Ctrl-G: toggling input grab!\n");
  mode = SDL_WM_GrabInput(SDL_GRAB_QUERY);
  IF (mode EQ SDL_GRAB_ON) THEN
    PRINT("Grab was on\n");
  ELSE
    PRINT("Grab was off\n");
  END_IF
  mode = SDL_WM_GrabInput(!mode);
  IF (mode EQ SDL_GRAB_ON) THEN
    PRINT("Grab is now on\n");
  ELSE
    PRINT("Grab is now off\n");
  END_IF
END_SUB

SUB HotKey_Iconify()
BEGIN_SUB
  PRINT("Ctrl-Z: iconifying window!\n");
  SDL_WM_IconifyWindow();
END_SUB

FUNCTION int HandleEvent(SDL_Event PTR event)
BEGIN_FUNCTION
  DIM AS int done;
  done = 0;
  SELECT_CASE(event->type)
    BEGIN_SELECT
      CASE SDL_ACTIVEEVENT:
        PRINT("app %s ", event->active.gain ? "gained" : "lost");
        IF (event->active.state & SDL_APPACTIVE) THEN
          PRINT("active ");
        ELSE_IF (event->active.state & SDL_APPMOUSEFOCUS) THEN
          PRINT("mouse ");
        ELSE_IF (event->active.state & SDL_APPINPUTFOCUS) THEN
          PRINT("input ");
        END_IF
        PRINT("focus\n");
        END_CASE
      CASE SDL_KEYDOWN:
        IF (event->key.keysym.sym EQ SDLK_ESCAPE) THEN_DO done = 1;
        IF ((event->key.keysym.sym EQ SDLK_g) AND (event->key.keysym.mod & KMOD_CTRL)) THEN_DO HotKey_ToggleGrab();
        IF ((event->key.keysym.sym EQ SDLK_z) AND (event->key.keysym.mod & KMOD_CTRL)) THEN_DO HotKey_Iconify();
        IF ((event->key.keysym.sym EQ SDLK_RETURN) AND (event->key.keysym.mod & KMOD_ALT)) THEN_DO HotKey_ToggleFullScreen();
        PRINT("key '%s' pressed\n",
        SDL_GetKeyName(event->key.keysym.sym));
        END_CASE
      CASE SDL_QUIT:
        done = 1;
        END_CASE
  END_SELECT
  RETURN_FUNCTION(done);
END_FUNCTION

SUB SDL_GL_Enter2DMode()
BEGIN_SUB
  DIM AS SDL_Surface PTR screen = SDL_GetVideoSurface();
  glPushAttrib(GL_ENABLE_BIT);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glViewport(0, 0, screen->w, screen->h);
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glOrtho(0.0, (GLdouble)screen->w, (GLdouble)screen->h, 0.0, 0.0, 1.0);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
END_SUB

SUB SDL_GL_Leave2DMode()
BEGIN_SUB
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glPopAttrib();
END_SUB

FUNCTION static int power_of_two(int input)
BEGIN_FUNCTION
  DIM AS int value = 1;
  WHILE (value < input)
  BEGIN_WHILE
    value <<= 1;
  WEND
  RETURN_FUNCTION(value);
END_FUNCTION

FUNCTION GLuint SDL_GL_LoadTexture(SDL_Surface PTR surface, GLfloat PTR texcoord)
BEGIN_FUNCTION
  DIM AS GLuint texture;
  DIM AS int w, h;
  DIM AS SDL_Surface PTR image;
  DIM AS SDL_Rect area;
  DIM AS Uint32 saved_flags;
  DIM AS Uint8 saved_alpha;
  w = power_of_two(surface->w);
  h = power_of_two(surface->h);
  texcoord[0] = 0.0f;
  texcoord[1] = 0.0f;
  texcoord[2] = (GLfloat)surface->w / w;
  texcoord[3] = (GLfloat)surface->h / h;
  image = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 32,
#if SDL_BYTEORDER EQ SDL_LIL_ENDIAN
    0x000000FF,
    0x0000FF00,
    0x00FF0000,
    0xFF000000
#else
    0xFF000000,
    0x00FF0000,
    0x0000FF00,
    0x000000FF
#endif
  );
  IF (image EQ NULL) THEN_DO RETURN_FUNCTION 0;
  saved_flags = surface->flags AT (SDL_SRCALPHA|SDL_RLEACCELOK);
  saved_alpha = surface->format->alpha;
  IF ((saved_flags & SDL_SRCALPHA) EQ SDL_SRCALPHA) THEN_DO SDL_SetAlpha(surface, 0, 0);
  area.x = 0;
  area.y = 0;
  area.w = surface->w;
  area.h = surface->h;
  SDL_BlitSurface(surface, AT area, image, AT area);
  IF ((saved_flags & SDL_SRCALPHA) EQ SDL_SRCALPHA) THEN_DO SDL_SetAlpha(surface, saved_flags, saved_alpha);
  glGenTextures(1, AT texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->pixels);
  SDL_FreeSurface(image);
  RETURN_FUNCTION(texture);
END_FUNCTION

SUB DrawLogoCursor()
BEGIN_SUB
  DIM AS static GLfloat texMinX, texMinY;
  DIM AS static GLfloat texMaxX, texMaxY;
  DIM AS static int w, h;
  DIM AS int x, y;
  IF (NOT cursor_texture) THEN
    DIM AS SDL_Surface PTR image;
    DIM AS GLfloat texcoord[4];
    image = SDL_LoadBMP(LOGO_FILE);
    IF (image EQ NULL) THEN_DO RETURN_FUNCTION;
    w = image->w;
    h = image->h;
    cursor_texture = SDL_GL_LoadTexture(image, texcoord);
    texMinX = texcoord[0];
    texMinY = texcoord[1];
    texMaxX = texcoord[2];
    texMaxY = texcoord[3];
    SDL_FreeSurface(image);
    IF (NOT cursor_texture) THEN_DO RETURN_FUNCTION;
  END_IF
  SDL_GetMouseState(AT x, AT y);
  x -= w/2;
  y -= h/2;
  SDL_GL_Enter2DMode();
  glBindTexture(GL_TEXTURE_2D, cursor_texture);
  glBegin(GL_TRIANGLE_STRIP);
  glTexCoord2f(texMinX, texMinY); glVertex2i(x,   y  );
  glTexCoord2f(texMaxX, texMinY); glVertex2i(x+w, y  );
  glTexCoord2f(texMinX, texMaxY); glVertex2i(x,   y+h);
  glTexCoord2f(texMaxX, texMaxY); glVertex2i(x+w, y+h);
  glEnd();
  SDL_GL_Leave2DMode();
END_SUB

SUB DrawLogoTexture()
BEGIN_SUB
  DIM AS static GLfloat texMinX, texMinY;
  DIM AS static GLfloat texMaxX, texMaxY;
  DIM AS static int x = 0;
  DIM AS static int y = 0;
  DIM AS static int w, h;
  DIM AS static int delta_x = 1;
  DIM AS static int delta_y = 1;
  DIM AS SDL_Surface PTR screen = SDL_GetVideoSurface();
  IF (NOT global_texture) THEN
    SDL_Surface PTR image;
    GLfloat texcoord[4];
    image = SDL_LoadBMP(LOGO_FILE);
    IF (image EQ NULL) THEN_DO RETURN_FUNCTION;
    w = image->w;
    h = image->h;
    global_texture = SDL_GL_LoadTexture(image, texcoord);
    texMinX = texcoord[0];
    texMinY = texcoord[1];
    texMaxX = texcoord[2];
    texMaxY = texcoord[3];
    SDL_FreeSurface(image);
    IF (NOT global_texture) RETURN_FUNCTION;
  END_IF
  x += delta_x;
  IF (x < 0) THEN
    x = 0;
    delta_x = -delta_x;
  ELSE_IF ((x+w) > screen->w) THEN
    x = screen->w-w;
    delta_x = -delta_x;
  END_IF
  y += delta_y;
  IF (y < 0) THEN
    y = 0;
    delta_y = -delta_y;
  ELSE_IF ((y+h) > screen->h) THEN
    y = screen->h-h;
    delta_y = -delta_y;
  END_IF
  SDL_GL_Enter2DMode();
  glBindTexture(GL_TEXTURE_2D, global_texture);
  glBegin(GL_TRIANGLE_STRIP);
  glTexCoord2f(texMinX, texMinY); glVertex2i(x, y);
  glTexCoord2f(texMaxX, texMinY); glVertex2i(x+w, y);
  glTexCoord2f(texMinX, texMaxY); glVertex2i(x, y+h);
  glTexCoord2f(texMaxX, texMaxY); glVertex2i(x+w, y+h);
  glEnd();
  SDL_GL_Leave2DMode();
END_SUB

SUB DrawLogoBlit()
BEGIN_SUB
  DIM AS static int x = 0;
  DIM AS static int y = 0;
  DIM AS static int w, h;
  DIM AS static int delta_x = 1;
  DIM AS static int delta_y = 1;
  DIM AS SDL_Rect dst;
  DIM AS SDL_Surface PTR screen = SDL_GetVideoSurface();
  IF (global_image EQ NULL) THEN
    DIM AS SDL_Surface PTR temp;
    temp = SDL_LoadBMP(LOGO_FILE);
    IF (temp EQ NULL) RETURN_FUNCTION;
    w = temp->w;
    h = temp->h;
    global_image = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h,
      screen->format->BitsPerPixel,
      screen->format->Rmask,
      screen->format->Gmask,
      screen->format->Bmask,
      screen->format->Amask);
    IF (global_image) THEN_DO SDL_BlitSurface(temp, NULL, global_image, NULL);
    SDL_FreeSurface(temp);
    IF (NOT global_image) THEN_DO RETURN_FUNCTION;
  END_IF
  x += delta_x;
  IF (x < 0) THEN
    x = 0;
    delta_x = -delta_x;
  ELSE_IF ((x+w) > screen->w) THEN
    x = screen->w-w;
    delta_x = -delta_x;
  END_IF
  y += delta_y;
  IF (y < 0) THEN
    y = 0;
    delta_y = -delta_y;
  ELSE_IF ((y+h) > screen->h) THEN
    y = screen->h-h;
    delta_y = -delta_y;
  END_IF
  dst.x = x;
  dst.y = y;
  dst.w = w;
  dst.h = h;
  SDL_BlitSurface(global_image, NULL, screen, AT dst);
  SDL_UpdateRects(screen, 1, AT dst);
END_SUB

FUNCTION int RunGLTest(int argc, char PTR argv[], int logo, int logocursor, int slowly, int bpp, float gamma, int noframe, int fsaa, int sync, int accel)
BEGIN_FUNCTION
  DIM AS int i;
  DIM AS int rgb_size[3];
  DIM AS int w = 640;
  DIM AS int h = 480;
  DIM AS int done = 0;
  DIM AS int frames;
  DIM AS Uint32 start_time, this_time;
  DIM AS float color[8][3] = {
    { 1.0,  1.0,  0.0},
    { 1.0,  0.0,  0.0},
    { 0.0,  0.0,  0.0},
    { 0.0,  1.0,  0.0},
    { 0.0,  1.0,  1.0},
    { 1.0,  1.0,  1.0},
    { 1.0,  0.0,  1.0},
    { 0.0,  0.0,  1.0}
    };
  DIM AS float cube[8][3] = {
    { 0.5,  0.5, -0.5},
    { 0.5, -0.5, -0.5},
    {-0.5, -0.5, -0.5},
    {-0.5,  0.5, -0.5},
    {-0.5,  0.5,  0.5},
    { 0.5,  0.5,  0.5},
    { 0.5, -0.5,  0.5},
    {-0.5, -0.5,  0.5}};
  DIM AS Uint32 video_flags;
  DIM AS int value;
  IF(SDL_Init(SDL_INIT_VIDEO) < 0) THEN
    PRINT_FILE(stderr,"Couldn't initialize SDL: %s\n",SDL_GetError());
    exit(1);
  END_IF
  IF (bpp == 0) THEN
    IF (SDL_GetVideoInfo()->vfmt->BitsPerPixel <= 8) THEN
      bpp = 8;
    ELSE
      bpp = 16;
    END_IF
  END_IF
  IF (logo AND USE_DEPRECATED_OPENGLBLIT) THEN
    video_flags = SDL_OPENGLBLIT;
  ELSE
    video_flags = SDL_OPENGL;
  END_IF
  FOR (i = 1 TO argv[i] STEP INCR i)
  BEGIN_FOR
    IF (strcmp(argv[i], "-fullscreen") EQ 0) THEN_DO video_flags |= SDL_FULLSCREEN;
  NEXT
  IF (noframe) THEN_DO video_flags |= SDL_NOFRAME;
  SELECT_CASE (bpp)
  BEGIN_SELECT
    CASE 8:
      rgb_size[0] = 3;
      rgb_size[1] = 3;
      rgb_size[2] = 2;
      END_CASE
    CASE 15:
    CASE 16:
      rgb_size[0] = 5;
      rgb_size[1] = 5;
      rgb_size[2] = 5;
      END_CASE
    CASE_ELSE
      rgb_size[0] = 8;
      rgb_size[1] = 8;
      rgb_size[2] = 8;
      END_CASE
  END_SELECT
  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, rgb_size[0]);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, rgb_size[1]);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, rgb_size[2]);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  IF (fsaa) THEN
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, fsaa);
  END_IF
  IF (accel) THEN_DO SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
  IF (sync) THEN
    SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 1);
  ELSE
    SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 0);
  END_IF
  IF (SDL_SetVideoMode(w, h, bpp, video_flags) EQ NULL) THEN
    PRINT_FILE(stderr, "Couldn't set GL mode: %s\n", SDL_GetError());
    SDL_Quit();
    exit(1);
  END_IF
  PRINT("Screen BPP: %d\n", SDL_GetVideoSurface()->format->BitsPerPixel);
  PRINT("\n");
  PRINT("Vendor     : %s\n", glGetString(GL_VENDOR));
  PRINT("Renderer   : %s\n", glGetString(GL_RENDERER));
  PRINT("Version    : %s\n", glGetString(GL_VERSION));
  PRINT("Extensions : %s\n", glGetString(GL_EXTENSIONS));
  PRINT("\n");
  SDL_GL_GetAttribute(SDL_GL_RED_SIZE, AT value);
  PRINT("SDL_GL_RED_SIZE: requested %d, got %d\n", rgb_size[0],value);
  SDL_GL_GetAttribute(SDL_GL_GREEN_SIZE, AT value);
  PRINT("SDL_GL_GREEN_SIZE: requested %d, got %d\n", rgb_size[1],value);
  SDL_GL_GetAttribute(SDL_GL_BLUE_SIZE, AT value);
  PRINT("SDL_GL_BLUE_SIZE: requested %d, got %d\n", rgb_size[2],value);
  SDL_GL_GetAttribute(SDL_GL_DEPTH_SIZE, AT value);
  PRINT("SDL_GL_DEPTH_SIZE: requested %d, got %d\n", bpp, value);
  SDL_GL_GetAttribute(SDL_GL_DOUBLEBUFFER, AT value);
  PRINT("SDL_GL_DOUBLEBUFFER: requested 1, got %d\n", value);
  IF (fsaa) THEN
    SDL_GL_GetAttribute(SDL_GL_MULTISAMPLEBUFFERS, AT value);
    PRINT("SDL_GL_MULTISAMPLEBUFFERS: requested 1, got %d\n", value);
    SDL_GL_GetAttribute(SDL_GL_MULTISAMPLESAMPLES, AT value);
    PRINT("SDL_GL_MULTISAMPLESAMPLES: requested %d, got %d\n", fsaa, value);
  END_IF
  IF (accel) THEN
    SDL_GL_GetAttribute(SDL_GL_ACCELERATED_VISUAL, AT value);
    PRINT("SDL_GL_ACCELERATED_VISUAL: requested 1, got %d\n", value);
  END_IF
  IF (sync) THEN
    SDL_GL_GetAttribute(SDL_GL_SWAP_CONTROL, AT value);
    printf("SDL_GL_SWAP_CONTROL: requested 1, got %d\n", value);
  END
  SDL_WM_SetCaption("C BASIC SDL 1.2 OpenGL", "testgl");
  IF (gamma != 0.0) THEN_DO SDL_SetGamma(gamma, gamma, gamma);
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-2.0, 2.0, -2.0, 2.0, -20.0, 20.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glShadeModel(GL_SMOOTH);
  start_time = SDL_GetTicks();
  frames = 0;
  WHILE(NOT done)
  BEGIN_WHILE
    GLenum gl_error;
    char PTR sdl_error;
    SDL_Event event;
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBegin(GL_QUADS);
#ifdef SHADED_CUBE
    glColor3fv(color[0]);
    glVertex3fv(cube[0]);
    glColor3fv(color[1]);
    glVertex3fv(cube[1]);
    glColor3fv(color[2]);
    glVertex3fv(cube[2]);
    glColor3fv(color[3]);
    glVertex3fv(cube[3]);
    glColor3fv(color[3]);
    glVertex3fv(cube[3]);
    glColor3fv(color[4]);
    glVertex3fv(cube[4]);
    glColor3fv(color[7]);
    glVertex3fv(cube[7]);
    glColor3fv(color[2]);
    glVertex3fv(cube[2]);
    glColor3fv(color[0]);
    glVertex3fv(cube[0]);
    glColor3fv(color[5]);
    glVertex3fv(cube[5]);
    glColor3fv(color[6]);
    glVertex3fv(cube[6]);
    glColor3fv(color[1]);
    glVertex3fv(cube[1]);
    glColor3fv(color[5]);
    glVertex3fv(cube[5]);
    glColor3fv(color[4]);
    glVertex3fv(cube[4]);
    glColor3fv(color[7]);
    glVertex3fv(cube[7]);
    glColor3fv(color[6]);
    glVertex3fv(cube[6]);
    glColor3fv(color[5]);
    glVertex3fv(cube[5]);
    glColor3fv(color[0]);
    glVertex3fv(cube[0]);
    glColor3fv(color[3]);
    glVertex3fv(cube[3]);
    glColor3fv(color[4]);
    glVertex3fv(cube[4]);
    glColor3fv(color[6]);
    glVertex3fv(cube[6]);
    glColor3fv(color[1]);
    glVertex3fv(cube[1]);
    glColor3fv(color[2]);
    glVertex3fv(cube[2]);
    glColor3fv(color[7]);
    glVertex3fv(cube[7]);
#else
    glColor3f(1.0, 0.0, 0.0);
    glVertex3fv(cube[0]);
    glVertex3fv(cube[1]);
    glVertex3fv(cube[2]);
    glVertex3fv(cube[3]);
    glColor3f(0.0, 1.0, 0.0);
    glVertex3fv(cube[3]);
    glVertex3fv(cube[4]);
    glVertex3fv(cube[7]);
    glVertex3fv(cube[2]);
    glColor3f(0.0, 0.0, 1.0);
    glVertex3fv(cube[0]);
    glVertex3fv(cube[5]);
    glVertex3fv(cube[6]);
    glVertex3fv(cube[1]);
    glColor3f(0.0, 1.0, 1.0);
    glVertex3fv(cube[5]);
    glVertex3fv(cube[4]);
    glVertex3fv(cube[7]);
    glVertex3fv(cube[6]);
    glColor3f(1.0, 1.0, 0.0);
    glVertex3fv(cube[5]);
    glVertex3fv(cube[0]);
    glVertex3fv(cube[3]);
    glVertex3fv(cube[4]);
    glColor3f(1.0, 0.0, 1.0);
    glVertex3fv(cube[6]);
    glVertex3fv(cube[1]);
    glVertex3fv(cube[2]);
    glVertex3fv(cube[7]);
#endif
    glEnd();
    glMatrixMode(GL_MODELVIEW);
    glRotatef(5.0, 1.0, 1.0, 1.0);
    IF (logo) THEN
      IF (USE_DEPRECATED_OPENGLBLIT) THEN
        DrawLogoBlit();
      ELSE
        DrawLogoTexture();
      END_IF
    END_IF
    IF (logocursor) THEN_DO DrawLogoCursor();
    SDL_GL_SwapBuffers();
    gl_error = glGetError();
    IF(gl_error != GL_NO_ERROR) THEN_DO PRINT_FILE(stderr, "testgl: OpenGL error: %d\n", gl_error);
    sdl_error = SDL_GetError();
    IF(sdl_error[0] != '\0') THEN
      PRINT_FILE(stderr, "testgl: SDL error '%s'\n", sdl_error);
      SDL_ClearError();
    END_IF
    IF (slowly) THEN_DO SDL_Delay(20);
    WHILE (SDL_PollEvent(AT event))
    BEGIN_WHILE
      done = HandleEvent(AT event);
    WEND
    INCR frames;
  WEND
  this_time = SDL_GetTicks();
  IF (this_time != start_time) THEN_DO PRINT("%2.2f FPS\n", ((float)frames/(this_time-start_time))*1000.0);
  IF (global_image) THEN
    SDL_FreeSurface(global_image);
    global_image = NULL;
  END_IF
  IF (global_texture) THEN
    glDeleteTextures(1, AT global_texture);
    global_texture = 0;
  END_IF
  IF (cursor_texture) THEN
    glDeleteTextures(1, AT cursor_texture);
    cursor_texture = 0;
  END_IF
  SDL_Quit();
  RETURN_FUNCTION(0);
END_FUNCTION

MAIN
BEGIN_FUNCTION
  DIM AS int i, logo, logocursor = 0;
  DIM AS int numtests;
  DIM AS int bpp = 0;
  DIM AS int slowly;
  DIM AS float gamma = 0.0;
  DIM AS int noframe = 0;
  DIM AS int fsaa = 0;
  DIM AS int accel = 0;
  DIM AS int sync = 0;
  logo = 0;
  slowly = 0;
  numtests = 1;
  FOR (i = 1 TO argv[i] STEP INCR i)
  BEGIN_FOR
    IF (strcmp(argv[i], "-twice") EQ 0) THEN_DO INCR numtests;
    IF (strcmp(argv[i], "-logo") EQ 0) THEN
      logo = 1;
      USE_DEPRECATED_OPENGLBLIT = SDL_FALSE;
    END_IF
    IF (strcmp(argv[i], "-logoblit") EQ 0) THEN
      logo = 1;
      USE_DEPRECATED_OPENGLBLIT = SDL_TRUE;
    END_IF
    IF (strcmp(argv[i], "-logocursor") EQ 0) THEN_DO logocursor = 1;
    IF (strcmp(argv[i], "-slow") EQ 0) THEN_DO slowly = 1;
    IF (strcmp(argv[i], "-bpp") EQ 0) THEN_DO bpp = atoi(argv[INCR i]);
    IF (strcmp(argv[i], "-gamma") EQ 0) THEN_DO gamma = (float)atof(argv[INCR i]);
    IF (strcmp(argv[i], "-noframe") EQ 0) THEN_DO noframe = 1;
    IF (strcmp(argv[i], "-fsaa") EQ 0) THEN_DO INCR fsaa;
    IF (strcmp(argv[i], "-accel") EQ 0) THEN_DO INCR accel;
    IF (strcmp(argv[i], "-sync") EQ 0) THEN_DO INCR sync;
    IF (strncmp(argv[i], "-h", 2) EQ 0) THEN
      PRINT("Usage: %s [-twice] [-logo] [-logocursor] [-slow] [-bpp n] [-gamma n] [-noframe] [-fsaa] [-accel] [-sync] [-fullscreen]\n", argv[0]);
      exit(0);
    END_IF
  NEXT
  FOR (i = 0 TO i<numtests STEP INCR i)
  BEGIN_FOR
    RunGLTest(argc, argv, logo, logocursor, slowly, bpp, gamma, noframe, fsaa, sync, accel);
  NEXT
  RETURN_FUNCTION(0);
END_FUNCTION

#else

MAIN
BEGIN_FUNCTION
  PRINT("No OpenGL support on this system\n");
  RETURN_FUNCTION(1);
END_FUNCTION

#endif
