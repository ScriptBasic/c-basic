// C BASIC SDL Draw example
// gcc -o sdldrawtest sdldrawtest.c -g -O2 -I/usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT -DHAVE_OPENGL -lSDL -lm -lSDL_draw

#ifdef WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#include "SDL.h"
#include "SDL_draw.h"

#include "cbasic.h"

FUNCTION Uint32 FastestFlags(Uint32 flags, unsigned int width, unsigned int height, unsigned int bpp)
BEGIN_FUNCTION
  DIM AS const SDL_VideoInfo PTR info;
  flags |= SDL_FULLSCREEN;
  info = SDL_GetVideoInfo();
  IF (info->blit_hw_CC AND info->blit_fill) THEN_DO flags |= SDL_HWSURFACE;
  IF ((flags & SDL_HWSURFACE) EQ SDL_HWSURFACE) THEN
    IF (info->video_mem * 1024 > (height * width * bpp / 8)) THEN
      flags |= SDL_DOUBLEBUF;
    ELSE
      flags &= ~SDL_HWSURFACE;
    END_IF
  END_IF
  RETURN_FUNCTION(flags);
END_FUNCTION

#ifdef WIN32
FUNCTION int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow)
BEGIN_FUNCTION
  RETURN_FUNCTION(main(__argc, __argv));
END_FUNCTION
#endif

MAIN
BEGIN_FUNCTION
  DIM AS SDL_Surface PTR screen;
  DIM AS int width, height;
  DIM AS Uint8  video_bpp;
  DIM AS Uint32 videoflags;
  DIM AS int done;
  DIM AS SDL_Event event;
  DIM AS Uint32 then, now, frames;
  IF (SDL_Init(SDL_INIT_VIDEO) < 0 ) THEN
    PRINT_FILE(stderr, "SDL_Init problem: %s", SDL_GetError());
    exit(1);
  END_IF
  atexit(SDL_Quit);
  videoflags = SDL_SWSURFACE | SDL_ANYFORMAT;
  width = 640;
  height = 480;
  video_bpp = 0;
  WHILE (argc > 1)
  BEGIN_WHILE
    DECR argc;
    IF (strcmp(argv[argc-1], "-width") EQ 0) THEN
      width = atoi(argv[argc]);
      DECR argc;
    ELSE_IF (strcmp(argv[argc-1], "-height") EQ 0) THEN
      height = atoi(argv[argc]);
      DECR argc;
    ELSE_IF (strcmp(argv[argc-1], "-bpp") EQ 0) THEN
      video_bpp = atoi(argv[argc]);
      videoflags &= ~SDL_ANYFORMAT;
      DECR argc;
    ELSE_IF (strcmp(argv[argc], "-fast") EQ 0) THEN
      videoflags = FastestFlags(videoflags, width, height, video_bpp);
    ELSE_IF (strcmp(argv[argc], "-hw") EQ 0) THEN
      videoflags ^= SDL_HWSURFACE;
    ELSE_IF (strcmp(argv[argc], "-flip") EQ 0 ) THEN
      videoflags ^= SDL_DOUBLEBUF;
    ELSE_IF (strcmp(argv[argc], "-fullscreen") EQ 0) THEN
      videoflags ^= SDL_FULLSCREEN;
    ELSE
      PRINT_FILE( stderr, "Use: %s [-bpp N] [-hw] [-flip] [-fast] [-fullscreen]\n", argv[0]);
      exit(1);
    END_IF
  WEND
  screen = SDL_SetVideoMode(width, height, video_bpp, videoflags);
  IF (NOT screen) THEN
    PRINT_FILE (stderr, "I can not activate video mode: %dx%d: %s\n", width, height, SDL_GetError());
    exit(2);
  END_IF
  SDL_WM_SetCaption("C BASIC SDL Graphic Primitives", 0);
  DIM AS Uint32 c_white = SDL_MapRGB(screen->format, 255, 255, 255);
  DIM AS Uint32 c_gray = SDL_MapRGB(screen->format, 200, 200, 200);
  DIM AS Uint32 c_dgray= SDL_MapRGB(screen->format, 64, 64, 64);
  DIM AS Uint32 c_cyan = SDL_MapRGB(screen->format, 32, 255, 255);
  Draw_Line(screen, 100, 100, 30, 0, c_white);
  Draw_Line(screen, 30, 0, 100, 100, c_white);
  Draw_Line(screen, 100, 100, 30, 0, c_white);
  Draw_Line(screen, 30, 0, 100, 100, c_white);
  Draw_Line(screen, 0, 0, 100, 100, c_white);
  Draw_Line(screen, 100, 100, 300, 200, c_white);
  Draw_Line(screen, 200, 300, 250, 400, SDL_MapRGB(screen->format, 128, 128, 255));
  Draw_Line(screen, 500, 50, 600, 70, SDL_MapRGB(screen->format, 128, 255, 128));
  Draw_Line(screen, 500, 50, 600, 70, SDL_MapRGB(screen->format, 128, 255, 128));
  Draw_Circle(screen, 100, 100, 50, c_white);
  Draw_Circle(screen, 150, 150, 5, c_white);
  Draw_Circle(screen, 150, 150, 4, SDL_MapRGB(screen->format, 64, 64, 64));
  Draw_Circle(screen, 150, 150, 3, SDL_MapRGB(screen->format, 255, 0, 0));
  Draw_Circle(screen, 150, 150, 2, SDL_MapRGB(screen->format, 0, 255, 0));
  Draw_Circle(screen, 150, 150, 1, SDL_MapRGB(screen->format, 0, 0, 255));
  Draw_Line(screen, 500, 100, 600, 120, SDL_MapRGB(screen->format, 128, 255, 128));
  Draw_Circle(screen, 601, 121, 2, c_white);
  Draw_Circle(screen, 400, 200, 2, c_white);
  Draw_Line(screen, 400, 200, 409, 200, c_white);
  Draw_Circle(screen, 409, 200, 2, c_white);
  Draw_Line(screen, 400, 200, 400, 250, c_white);
  Draw_Circle(screen, 400, 250, 2, c_white);
  Draw_Line(screen, 409, 200, 400, 250, c_white);
  Draw_Line(screen, 400, 300, 409, 300, c_gray);
  Draw_Line(screen, 400, 300, 400, 350, c_gray);
  Draw_Line(screen, 409,300, 400, 350, c_dgray);
  Draw_Rect(screen, 398, 298, 4, 4, c_cyan);
  Draw_Rect(screen, 407, 298, 4, 4, c_cyan);
  Draw_Rect(screen, 398, 348, 4, 4, c_cyan);
  Draw_HLine(screen, 10, 400, 50, c_white);
  Draw_VLine(screen, 60, 400, 360, c_white);
  Draw_Rect(screen, 500, 400, 50, 50, c_white);
  Draw_Pixel(screen, 510, 410, c_white);
  Draw_Pixel(screen, 520, 420, SDL_MapRGB(screen->format, 255, 0, 0));
  Draw_Pixel(screen, 530, 430, SDL_MapRGB(screen->format, 0, 255, 0));
  Draw_Pixel(screen, 540,440, SDL_MapRGB(screen->format, 0, 0, 255));
  Draw_Ellipse(screen, 100, 300, 60, 30, c_white);
  Draw_FillEllipse(screen, 300, 300, 30, 60, SDL_MapRGB(screen->format, 64, 64, 200));
  Draw_Ellipse(screen, 300, 300, 30, 60, SDL_MapRGB(screen->format, 255, 0, 0));
  Draw_Round(screen, 200, 20, 70, 50, 10, c_white);
  Draw_Round(screen, 300, 20, 70, 50, 20, SDL_MapRGB(screen->format, 255, 0, 0));
  Draw_FillRound(screen, 390, 20, 70, 50, 20, SDL_MapRGB(screen->format, 255, 0, 0));
  Draw_Round(screen, 390, 20, 70, 50, 20, c_cyan);
  Draw_Rect(screen, 499, 199, 52, 72, SDL_MapRGB(screen->format, 255, 255, 0));
  Draw_FillRect(screen, 500, 200, 50, 70, SDL_MapRGB(screen->format, 64, 200, 64));
  Draw_FillCircle(screen, 500, 330, 30, c_cyan);
  SDL_UpdateRect(screen, 0, 0, 0, 0);
  frames = 0;
  then = SDL_GetTicks();
  done = 0;
  WHILE (NOT done)
  BEGIN_WHILE
    INCR frames;
    WHILE (SDL_PollEvent(AT event))
    BEGIN_WHILE
      SELECT_CASE (event.type)
      BEGIN_SELECT
        CASE SDL_KEYDOWN:
        CASE SDL_QUIT:
          done = 1;
          END_CASE
        CASE_ELSE
          END_CASE
      END_SELECT
    WEND
  WEND
  now = SDL_GetTicks();
  IF (now > then) THEN_DO PRINT("%2.2f frames per second\n", ((double)frames * 1000) / (now - then));
  PRINT_FILE(stderr, "[END]\n");
  RETURN_FUNCTION(0);
END_FUNCTION
