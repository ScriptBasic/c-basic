/* C BASIC - SDL 1.2:  Move N sprites around on the screen as fast as possible */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include "SDL.h"
#include "cbasic.h"

#define NUM_SPRITES 100
#define MAX_SPEED   1

DIM AS SDL_Surface PTR sprite;
DIM AS int numsprites;
DIM AS SDL_Rect PTR sprite_rects;
DIM AS SDL_Rect PTR positions;
DIM AS SDL_Rect PTR velocities;
DIM AS int sprites_visible;
DIM AS int debug_flip;
DIM AS Uint16 sprite_w, sprite_h;

static SUB quit(int rc)
BEGIN_SUB
  SDL_Quit();
  exit(rc);
END_SUB

FUNCTION int LoadSprite(char PTR file)
BEGIN_FUNCTION
  DIM AS SDL_Surface PTR temp;
  sprite = SDL_LoadBMP(file);
  IF (sprite EQ NULL) THEN
    PRINT_FILE (stderr, "Couldn't load %s: %s", file, SDL_GetError());
    RETURN_FUNCTION(-1);
  END_IF
  IF (sprite->format->palette) THEN
    SDL_SetColorKey(sprite, (SDL_SRCCOLORKEY|SDL_RLEACCEL), PTR (Uint8 PTR)sprite->pixels);
  END_IF
  temp = SDL_DisplayFormat(sprite);
  SDL_FreeSurface(sprite);
  IF (temp EQ NULL) THEN
    PRINT_FILE (stderr, "Couldn't convert background: %s\n", SDL_GetError());
    RETURN_FUNCTION(-1);
  END_IF
  sprite = temp;
  RETURN_FUNCTION(0);
END_FUNCTION

SUB MoveSprites(SDL_Surface PTR screen, Uint32 background)
BEGIN_SUB
  DIM AS int i, nupdates;
  DIM AS SDL_Rect area, PTR position, PTR velocity;
  nupdates = 0;
  IF (sprites_visible) THEN
    SDL_FillRect(screen, NULL, background);
  END_IF
  FOR (i=0 TO i<numsprites STEP INCR i)
  BEGIN_FOR
    position = AT positions[i];
    velocity = AT velocities[i];
    position->x += velocity->x;
    IF ((position->x < 0) OR (position->x >= (screen->w - sprite_w))) THEN
      velocity->x = -velocity->x;
      position->x += velocity->x;
    END_IF
    position->y += velocity->y;
    IF ((position->y < 0) OR (position->y >= (screen->h - sprite_w)) ) THEN
      velocity->y = -velocity->y;
      position->y += velocity->y;
    END_IF
    area = PTR position;
    SDL_BlitSurface(sprite, NULL, screen, AT area);
    sprite_rects[INCR nupdates] = area;
  NEXT
  IF (debug_flip) THEN
    IF ((screen->flags & SDL_DOUBLEBUF) EQ SDL_DOUBLEBUF) THEN
      static int t = 0;
      Uint32 color = SDL_MapRGB (screen->format, 255, 0, 0);
      DIM AS SDL_Rect r;
      r.x = (sin((float)t * 2 * 3.1459) + 1.0) / 2.0 * (screen->w-20);
      r.y = 0;
      r.w = 20;
      r.h = screen->h;
      SDL_FillRect(screen, AT r, color);
      t += 2;
    END_IF
  END_IF
  IF ((screen->flags & SDL_DOUBLEBUF) EQ SDL_DOUBLEBUF) THEN
    SDL_Flip(screen);
  ELSE
    SDL_UpdateRects(screen, nupdates, sprite_rects);
  END_IF
  sprites_visible = 1;
END_SUB

FUNCTION Uint32 FastestFlags(Uint32 flags, int width, int height, int bpp)
BEGIN_FUNCTION
  DIM AS const SDL_VideoInfo PTR info;
  flags |= SDL_FULLSCREEN;
  info = SDL_GetVideoInfo();
  IF (info->blit_hw_CC AND info->blit_fill) THEN
    flags |= SDL_HWSURFACE;
  END_IF
  IF ((flags & SDL_HWSURFACE) EQ SDL_HWSURFACE ) THEN
    IF (info->video_mem*1024 > (height*width*bpp/8) ) THEN
      flags |= SDL_DOUBLEBUF;
    ELSE
      flags &= ~SDL_HWSURFACE;
    END_IF
  END_IF
  RETURN_FUNCTION(flags);
END_FUNCTION

MAIN
BEGIN_FUNCTION
  DIM AS SDL_Surface PTR screen;
  DIM AS Uint8 PTR mem;
  DIM AS int width, height;
  DIM AS Uint8  video_bpp;
  DIM AS Uint32 videoflags;
  DIM AS Uint32 background;
  DIM AS int i, done;
  DIM AS SDL_Event event;
  DIM AS Uint32 start, now, frames;
  IF (SDL_Init(SDL_INIT_VIDEO) < 0) THEN
    PRINT_FILE (stderr, "Couldn't initialize SDL: %s\n",SDL_GetError());
    RETURN_FUNCTION(1);
  END_IF
  numsprites = NUM_SPRITES;
  videoflags = SDL_SWSURFACE|SDL_ANYFORMAT;
  width = 640;
  height = 480;
  video_bpp = 8;
  debug_flip = 0;
  WHILE (argc > 1)
  BEGIN_WHILE
    DECR argc;
    IF (strcmp(argv[argc-1], "-width") EQ 0 ) THEN
      width = atoi(argv[argc]);
      DECR argc;
    ELSE_IF (strcmp(argv[argc-1], "-height") EQ 0 ) THEN
      height = atoi(argv[argc]);
      DECR argc;
    ELSE_IF (strcmp(argv[argc-1], "-bpp") EQ 0 ) THEN
      video_bpp = atoi(argv[argc]);
      videoflags &= ~SDL_ANYFORMAT;
      DECR argc;
    ELSE_IF (strcmp(argv[argc], "-fast") EQ 0 ) THEN
      videoflags = FastestFlags(videoflags, width, height, video_bpp);
    ELSE_IF (strcmp(argv[argc], "-hw") EQ 0 ) THEN
      videoflags ^= SDL_HWSURFACE;
    ELSE_IF (strcmp(argv[argc], "-flip") EQ 0 ) THEN
      videoflags ^= SDL_DOUBLEBUF;
    ELSE_IF (strcmp(argv[argc], "-debugflip") EQ 0 ) THEN
      debug_flip ^= 1;
    ELSE_IF (strcmp(argv[argc], "-fullscreen") EQ 0 ) THEN
      videoflags ^= SDL_FULLSCREEN;
    ELSE_IF (isdigit(argv[argc][0]) ) THEN
      numsprites = atoi(argv[argc]);
    ELSE
      PRINT_FILE (stderr,"Usage: %s [-bpp N] [-hw] [-flip] [-fast] [-fullscreen] [numsprites]\n",argv[0]);
      quit(1);
    END_IF
  WEND
  screen = SDL_SetVideoMode(width, height, video_bpp, videoflags);
  IF (NOT screen) THEN
    PRINT_FILE (stderr, "Couldn't set %dx%d video mode: %s\n",width, height, SDL_GetError());
    quit(2);
  END_IF
  SDL_WM_SetCaption("C BASIC SDL 1.2 Sprites",0);
  IF (LoadSprite("icon.bmp") < 0) THEN
    quit(1);
  END_IF
  mem = (Uint8 PTR)malloc(4 * sizeof(SDL_Rect)PTR numsprites);
  IF (mem EQ NULL) THEN
    SDL_FreeSurface(sprite);
    PRINT_FILE (stderr, "Out of memory!\n");
    quit(2);
  END_IF
  sprite_rects = (SDL_Rect PTR)mem;
  positions = sprite_rects;
  sprite_rects += numsprites;
  velocities = sprite_rects;
  sprite_rects += numsprites;
  sprite_w = sprite->w;
  sprite_h = sprite->h;
  srand(time(NULL));
  FOR (i=0 TO i<numsprites STEP INCR i)
  BEGIN_FOR
    positions[i].x = rand() MOD (screen->w - sprite_w);
    positions[i].y = rand() MOD (screen->h - sprite_h);
    positions[i].w = sprite->w;
    positions[i].h = sprite->h;
    velocities[i].x = 0;
    velocities[i].y = 0;
    WHILE (NOT velocities[i].x AND NOT velocities[i].y )
    BEGIN_WHILE
      velocities[i].x = (rand() MOD (MAX_SPEED*2+1))-MAX_SPEED;
      velocities[i].y = (rand() MOD (MAX_SPEED*2+1))-MAX_SPEED;
    WEND
  NEXT
  background = SDL_MapRGB(screen->format, 0x00, 0x00, 0x00);
  PRINT ("Screen is at %d bits per pixel\n",screen->format->BitsPerPixel);
  IF ((screen->flags & SDL_HWSURFACE) EQ SDL_HWSURFACE) THEN
    PRINT ("Screen is in video memory\n");
  ELSE
    PRINT ("Screen is in system memory\n");
  END_IF
  IF ((screen->flags & SDL_DOUBLEBUF) EQ SDL_DOUBLEBUF) THEN
    PRINT ("Screen has double-buffering enabled\n");
  END_IF
  IF ((sprite->flags & SDL_HWSURFACE) EQ SDL_HWSURFACE ) THEN
    PRINT ("Sprite is in video memory\n");
  ELSE
    PRINT ("Sprite is in system memory\n");
  END_IF
  DIM AS SDL_Rect dst;
  dst.x = 0;
  dst.y = 0;
  dst.w = sprite->w;
  dst.h = sprite->h;
  SDL_BlitSurface(sprite, NULL, screen, AT dst);
  SDL_FillRect(screen, AT dst, background);
  IF ((sprite->flags & SDL_HWACCEL) EQ SDL_HWACCEL ) THEN
    PRINT ("Sprite blit uses hardware acceleration\n");
  END_IF
  IF ((sprite->flags & SDL_RLEACCEL) EQ SDL_RLEACCEL ) THEN
    PRINT ("Sprite blit uses RLE acceleration\n");
  END_IF
  frames = 0;
  start = SDL_GetTicks();
  done = 0;
  sprites_visible = 0;
  WHILE (NOT done)
  BEGIN_WHILE
    INCR frames;
    WHILE (SDL_PollEvent(AT event))
    BEGIN_WHILE
      SELECT_CASE(event.type)
      BEGIN_SELECT
        CASE SDL_MOUSEBUTTONDOWN:
          SDL_WarpMouse(screen->w/2, screen->h/2);
          END_CASE
        CASE SDL_KEYDOWN:
        CASE SDL_QUIT:
          done = 1;
          END_CASE
        CASE_ELSE
          END_CASE
      END_SELECT
    WEND
    MoveSprites(screen, background);
  WEND
  SDL_FreeSurface(sprite);
  free(mem);
  now = SDL_GetTicks();
  IF (now > start) THEN
    PRINT ("%2.2f frames per second\n",((double)frames*1000)/(now-start));
  END_IF
  SDL_Quit();
  RETURN_FUNCTION(0);
END_FUNCTION

