// C BASIC - SQLite 3
// gcc sql3.c -lsqlite3 -o cbsql3

#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include "cbasic.h"

CB_FUNCTION callback(void PTR NotUsed, int argc, char PTR PTR argv, char PTR PTR azColName)
BEGIN_FUNCTION
  DIM AS int i;
  FOR (i = 0 TO i<argc STEP INCR i)
  BEGIN_FOR
    PRINT ("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
  NEXT
  PRINT ("\n");
  RETURN_FUNCTION(0);
END_FUNCTION

MAIN
BEGIN_FUNCTION
  DIM AS sqlite3 PTR db;
  DIM AS char PTR zErrMsg = 0;
  DIM AS int rc;
  DIM AS char PTR sql;
  rc = sqlite3_open("test.db", AT db);
  IF (rc) THEN
    PRINT_FILE (stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
    exit(0);
  ELSE
    PRINT_FILE (stderr, "Opened database successfully\n");
  END_IF
  sql = "CREATE TABLE COMPANY("  \
        "ID INT PRIMARY KEY     NOT NULL," \
        "NAME           TEXT    NOT NULL," \
        "AGE            INT     NOT NULL," \
        "ADDRESS        CHAR(50)," \
        "SALARY         REAL );";
  rc = sqlite3_exec(db, sql, callback, 0, AT zErrMsg);
  IF (rc != SQLITE_OK) THEN
    PRINT_FILE (stderr, "SQL error: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
  ELSE
    PRINT_FILE (stdout, "Table created successfully\n");
  END_IF
  sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "  \
        "VALUES (1, 'Paul', 32, 'California', 20000.00 ); " \
        "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "  \
        "VALUES (2, 'Allen', 25, 'Texas', 15000.00 ); "     \
        "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)" \
        "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );" \
        "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)" \
        "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );";
  rc = sqlite3_exec(db, sql, callback, 0, AT zErrMsg);
  IF (rc != SQLITE_OK) THEN
    PRINT_FILE (stderr, "SQL error: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
  ELSE
    PRINT_FILE (stdout, "Records created successfully\n");
  END_IF
  sql = "SELECT * from COMPANY";
  rc = sqlite3_exec(db, sql, callback, 0, AT zErrMsg);
  IF (rc != SQLITE_OK) THEN
    PRINT_FILE (stderr, "SQL error: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
  ELSE
    PRINT_FILE (stdout, "Operation completed successfully\n");
  END_IF
  sqlite3_close(db);
  RETURN_FUNCTION(0);
END_FUNCTION
