// C BASIC - ScriptBasic IDE - (the beginning)
// gcc sb_embed.c -I/home/jrs/sb/source -lscriba -lpthread -o cbi

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "scriba.h"
#include "cbasic.h"

MAIN
BEGIN_FUNCTION
  DIM AS pSbProgram pProgram;
  pProgram = scriba_new(malloc,free);
  scriba_SetFileName(pProgram, argv[1]);
  scriba_LoadSourceProgram(pProgram);
  scriba_Run(pProgram, argv[2]);
  scriba_destroy(pProgram);
  RETURN_FUNCTION(0);
END_FUNCTION
